//
//  QuestionBot.swift
//  Button112
//
//  Created by Олег Мельник on 06.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

import UIKit

class QuestionBot: NSObject {
    
    static let sharedBot = QuestionBot()
    
    var currentQuestion = "Что случилось?"
    var currentAnswers : [String]? = ["Пожар", "Задымление", "Запах газа"]
    
    func generateQuestion(answer: String) {
        switch answer {
        case "Пожар":
            currentQuestion = "Что горит?"
            currentAnswers = ["Квартира", "Мусорка", "Гараж"]
        case "Задымление":
            currentQuestion = "Что дымит?"
            currentAnswers = ["Квартира", "Мусорка", "Гараж"]
        case "Запах газа":
            currentQuestion = "Откуда запах?"
            currentAnswers = ["Кухня", "Подъезд"]
        default:
            currentQuestion = "Ждите, мы едем!"
            currentAnswers = nil
        }
    }
}
