//
//  ViewController.swift
//  Button112
//
//  Created by Олег Мельник on 01.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

enum ContentType {
    case Question
    case Answer
    case Unknown
}

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let messagesTableView = UITableView()
    let bottomBar = UIView()
    var content : [AnyObject]! // array of questions/answers
    var bot = QuestionBot()

    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: false)
        self.title = "Служба 112"
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 0, green: 87/255, blue: 142/255, alpha: 1)
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
        messagesTableView.translatesAutoresizingMaskIntoConstraints = false
        messagesTableView.dataSource = self
        messagesTableView.delegate = self
        messagesTableView.separatorColor = UIColor.clearColor()
        self.view.addSubview(messagesTableView)
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[tableView]|", options: [], metrics: nil, views: ["tableView": messagesTableView]))
        

        
        
        setUpTopBar()
        setUpBottomBar()
        setUpAccessoryView()
        
        content = [AnyObject]()
        content.append(bot.currentQuestion)
        content.append(bot.currentAnswers!)
    }
    
    func setUpTopBar() {
        let menuButton = UIButton(type: .Custom)
        menuButton.frame = CGRectMake(0, 0, 20, 20)
        menuButton.setImage(UIImage(named: "menu_button"), forState: .Normal)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: menuButton)
        
        let phoneButton = UIButton(type: .Custom)
        phoneButton.frame = CGRectMake(0, 0, 20, 20)
        phoneButton.setImage(UIImage(named: "phone_button"), forState: .Normal)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: phoneButton)
    }
    
    func setUpBottomBar() {
        bottomBar.backgroundColor = UIColor(red: 0, green: 87/255, blue: 142/255, alpha: 1)
        bottomBar.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(bottomBar)
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|[bottomBar]|", options: [], metrics: nil, views: ["bottomBar": bottomBar]))
        self.view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView][bottomBar(30)]|", options: [], metrics: nil, views: ["tableView": messagesTableView, "bottomBar":bottomBar]))
    }
    
    func setUpAccessoryView() {
        let photoButton = UIButton(type: .Custom)
        photoButton.setImage(UIImage(named: "camera_button"), forState: .Normal)
        photoButton.translatesAutoresizingMaskIntoConstraints = false
        
        let otherButton = UIButton(type: .Custom)
        otherButton.setBackgroundImage((UIImage(named: "other_button"))?.resizableImageWithCapInsets(UIEdgeInsetsMake(0, 5, 0, 5)), forState: .Normal)
        otherButton.setTitle("Другое", forState: .Normal)
        otherButton.setTitleColor(UIColor.blackColor(), forState: .Normal)
        otherButton.titleLabel?.font = UIFont.systemFontOfSize(14)
        otherButton.translatesAutoresizingMaskIntoConstraints = false
        
        let micButton = UIButton(type: .Custom)
        micButton.setImage(UIImage(named: "mic_button"), forState: .Normal)
        micButton.translatesAutoresizingMaskIntoConstraints = false
        
        bottomBar.addSubview(photoButton)
        bottomBar.addSubview(otherButton)
        bottomBar.addSubview(micButton)
        
        for button in [photoButton, otherButton, micButton] {
            bottomBar.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(5)-[button]-(5)-|", options: [], metrics: nil, views: ["button": button]))
        }
        bottomBar.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-(5)-[photoButton(20)]-[otherButton]-[micButton(20)]-(5)-|", options: [], metrics: nil, views: ["photoButton": photoButton, "otherButton": otherButton, "micButton": micButton]))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func typeForContent(content: AnyObject) -> ContentType {
        if content is [String] {
            return .Answer
        }
        else if content is String {
            return .Question
        }
        return .Unknown
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return content!.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if typeForContent(content[indexPath.row]) == .Answer {
            let curContent = content[indexPath.row] as! [String]
            return 40 * CGFloat(curContent.count)
        }
        return 50
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "cell\(indexPath.row)"
        var cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier)
        if cell == nil {
            let handler : TouchUpHandler = {[weak self] _, title in
                self?.bot.generateQuestion(title!)
                self?.content.append((self?.bot.currentQuestion)!)
                if self?.bot.currentAnswers != nil {
                    self?.content.append((self?.bot.currentAnswers)!)
                }
                self?.messagesTableView.reloadData()
                self?.messagesTableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self!.content.count-1, inSection: 0), atScrollPosition: .Bottom, animated: false)
            }

            if typeForContent(content[indexPath.row]) == .Answer {
                cell = AnswerCell(content: content[indexPath.row] as! [String], reuseIdentifier: cellIdentifier, touchUpHandler: handler)
            }
            else {
                cell = QuestionCell(content: content[indexPath.row] as! String, reuseIdentifier: cellIdentifier, touchUpHandler: handler)
            }
            cell!.selectionStyle = .None
        }
        return cell!
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
}

