//
//  RadioGroup.swift
//  Button112
//
//  Created by Олег Мельник on 03.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

import UIKit

typealias TouchUpHandler = (Int, String?) -> Void

class RadioGroup: UIView {
    
    var buttons = [RadioButton]()
    var touchUpHandler : TouchUpHandler?
    var clickable = true
    var clickableOnce = false
    var fontColor = UIColor.whiteColor()
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    init(values: [String]) {
        super.init(frame: CGRectZero)
        self.backgroundColor = UIColor.clearColor()
        for value in values {
            configureButtonWithValue(value)
        }
    }
    
    func configureButtonWithValue(value: String) {
        let button = NSBundle.mainBundle().loadNibNamed("RadioButton", owner: self, options: nil).first as! RadioButton
        button.valueLabel.text = value
        button.valueLabel.textColor = self.fontColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleTouchUp), forControlEvents: .TouchUpInside)
        self.addSubview(button)
        buttons.append(button)
        addConstraintsForButton(button)
        
    }
    
    func addConstraintsForButton(button: RadioButton) {
        let buttonIndex = buttons.indexOf(button)
        self.addConstraint(NSLayoutConstraint(item: button, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1.0, constant: CGFloat(buttonIndex!) * 30.0))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[button]-|", options: [], metrics: nil, views: ["button": button]))
        self.addConstraint(NSLayoutConstraint(item: button, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 30))
    }
    
    func handleTouchUp(sender: AnyObject) {
        if clickable {
            let touchedButton = sender as! RadioButton
            let touchedIndex = buttons.indexOf(touchedButton)!
            for button in buttons {
                button.stateImage.image = buttons.indexOf(button) == touchedIndex ? UIImage(named: "radio_checked") : UIImage(named:"radio_unchecked")
            }
            touchUpHandler?(touchedIndex, touchedButton.valueLabel.text)
            if clickableOnce {
                clickable = false
            }
        }
        
    }
}
