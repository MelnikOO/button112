//
//  AnswerCell.swift
//  Button112
//
//  Created by Олег Мельник on 06.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

import UIKit

class AnswerCell: BubbleTableViewCell {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    convenience init(content: [String], reuseIdentifier: String?, touchUpHandler : TouchUpHandler?) {
        self.init(style: .Default, reuseIdentifier: reuseIdentifier)
        self.touchUpHandler = touchUpHandler
        self.bubbleContent = content
        self.addConstraint(NSLayoutConstraint(item: bubbleView, attribute: .Trailing, relatedBy: .Equal, toItem: self, attribute: .Trailing, multiplier: 1.0, constant: 0))
        bubbleImageView.image = UIImage(named: "bubble_answer")?.resizableImageWithCapInsets(UIEdgeInsetsMake(20, 20, 20, 20))
        self.addConstraint(NSLayoutConstraint(item: timeLabel, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 20.0))
        setUpContent()
    }
    
    func setUpContent() {
        let radioGroup = RadioGroup(values: self.bubbleContent as! [String])
        radioGroup.clickableOnce = true
        radioGroup.translatesAutoresizingMaskIntoConstraints = false
//        radioGroup.touchUpHandler = {[weak self] _, title in
//            bot.generateQuestion(title!)
//            content.append((self?.bot.currentQuestion)!)
//            if self?.bot.currentAnswers != nil {
//                self?.content.append((self?.bot.currentAnswers)!)
//            }
//            self?.messagesTableView.reloadData()
//            self?.messagesTableView.scrollToRowAtIndexPath(NSIndexPath(forRow: self!.content.count-1, inSection: 0), atScrollPosition: .Bottom, animated: false)
//        }
        radioGroup.touchUpHandler = self.touchUpHandler
        bubbleView.addSubview(radioGroup)
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[radioGroup]-|", options: [], metrics: nil, views: ["radioGroup": radioGroup]))
        bubbleView.addConstraint(NSLayoutConstraint(item: radioGroup, attribute: .CenterY, relatedBy: .Equal, toItem: bubbleView, attribute: .CenterY, multiplier: 1.0, constant: 0.0))
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[radioGroup]|", options: [], metrics: nil, views: ["radioGroup": radioGroup]))
    }

}
