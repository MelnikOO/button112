//
//  QuestionCell.swift
//  Button112
//
//  Created by Олег Мельник on 06.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

import UIKit

class QuestionCell: BubbleTableViewCell {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    convenience init(content: String, reuseIdentifier: String?, touchUpHandler : TouchUpHandler?) {
        self.init(style: .Default, reuseIdentifier: reuseIdentifier)
        self.touchUpHandler = touchUpHandler
        self.bubbleContent = content
        self.addConstraint(NSLayoutConstraint(item: bubbleView, attribute: .Leading, relatedBy: .Equal, toItem: self, attribute: .Leading, multiplier: 1.0, constant: 0))
        bubbleImageView.image = UIImage(named: "bubble_question")?.resizableImageWithCapInsets(UIEdgeInsetsMake(20, 20, 20, 20))
        self.addConstraint(NSLayoutConstraint(item: timeLabel, attribute: .Trailing, relatedBy: .Equal, toItem: self, attribute: .Trailing, multiplier: 1.0, constant: 20.0))
        setUpContent()
    }
    
    func setUpContent() {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = self.bubbleContent as? String
        label.font = UIFont.systemFontOfSize(14)
        bubbleView.addSubview(label)
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-(20)-[label]-|", options: [], metrics: nil, views: ["label": label]))
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[label]-|", options: [], metrics: nil, views: ["label": label]))
    }
}
