//
//  BubbleTableViewCell.swift
//  Button112
//
//  Created by Олег Мельник on 06.05.16.
//  Copyright © 2016 Oleg Melnik. All rights reserved.
//

import UIKit

class BubbleTableViewCell: UITableViewCell {
    
    let bubbleView = UIView()
    let timeLabel = UILabel()
    let bubbleImageView = UIImageView()
    var bubbleContent : AnyObject!
    var touchUpHandler : TouchUpHandler?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        bubbleView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(bubbleView)
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[bubbleView]-|", options: [], metrics: nil, views: ["bubbleView":bubbleView]))
        self.addConstraint(NSLayoutConstraint(item: bubbleView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 0.7, constant: 0))
        bubbleImageView.translatesAutoresizingMaskIntoConstraints = false
        bubbleView.addSubview(bubbleImageView)
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("|-[bubbleImageView]-|", options: [], metrics: nil, views: ["bubbleImageView":bubbleImageView]))
        bubbleView.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[bubbleImageView]|", options: [], metrics: nil, views: ["bubbleImageView":bubbleImageView]))
        setUpTimeLabel()

    }
    
    func setUpTimeLabel() {
        let now = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "hh:mm"
        formatter.timeZone = NSTimeZone.systemTimeZone()
        
        timeLabel.translatesAutoresizingMaskIntoConstraints = false
        timeLabel.text = formatter.stringFromDate(now)
        timeLabel.font = UIFont.systemFontOfSize(14)
        self.addSubview(timeLabel)
        self.addConstraint(NSLayoutConstraint(item: timeLabel, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: 70))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-[timeLabel(30)]", options: [], metrics: nil, views: ["timeLabel": timeLabel]))
    }

}
